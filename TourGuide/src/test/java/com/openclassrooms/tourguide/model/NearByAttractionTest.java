package com.openclassrooms.tourguide.model;

import jakarta.validation.Validator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * NearByAttractionTest is the unit test class managing the NearByAttraction
 *
 * @author MC
 * @version 1.0
 */
@SpringBootTest
class NearByAttractionTest {

    @Autowired
    private Validator validator;

    private TestConstraintViolation<NearByAttraction> testConstraintViolation;

    private NearByAttraction nearByAttraction;

    @BeforeEach
    public void setUpBefore() {
        testConstraintViolation = new TestConstraintViolation<>(validator);

        nearByAttraction = new NearByAttraction("Roger Dean Stadium", 26.890959, -80.116577, -41.490102, -13.738881, 6340.588384814216, 32);
    }

    // -----------------------------------------------------------------------------------------------
    // builder method
    // -----------------------------------------------------------------------------------------------
    @Test
    void builder_TestBuildAndNew_thenEqual() {
        // GIVEN
        // WHEN
        NearByAttraction objBuild = NearByAttraction.builder()
                .build();
        NearByAttraction objNew = new NearByAttraction();
        // THEN
        assertThat(objBuild).isEqualTo(objNew);
    }

    // -----------------------------------------------------------------------------------------------
    // attractionName attribute
    // -----------------------------------------------------------------------------------------------
    @Test
    void attractionName_normal_thenNoConstraintViolation() {
        // GIVEN
        // WHEN
        nearByAttraction.setAttractionName("AttractionName Test");
        // THEN
        String[][] errorList = {};
        testConstraintViolation.checking(nearByAttraction, errorList);
        assertThat(nearByAttraction.getAttractionName()).isEqualTo("AttractionName Test");
    }

    private static Stream<Arguments> listOfAttractionNameToTest() {
        String[][] errorSpace = {{"attractionName", "{jakarta.validation.constraints.NotBlank.message}"}};
        String[][] errorEmpty = {{"attractionName", "{jakarta.validation.constraints.NotBlank.message}"}};
        String[][] errorNull = {{"attractionName", "{jakarta.validation.constraints.NotBlank.message}"}};

        return Stream.of(
                Arguments.of(" ", errorSpace, "space")
                , Arguments.of("", errorEmpty, "empty")
                , Arguments.of(null, errorNull, "null")
        );
    }

    @ParameterizedTest(name = "AttractionName is {2} ({0}).")
    @MethodSource("listOfAttractionNameToTest")
    void attractionName_thenConstraintViolation(String attractionName, String[][] errorList, String message) {
        // GIVEN
        // WHEN
        nearByAttraction.setAttractionName(attractionName);
        // THEN
        testConstraintViolation.checking(nearByAttraction, errorList);
    }

    // -----------------------------------------------------------------------------------------------
    // attractionLatitude attribute
    // -----------------------------------------------------------------------------------------------
    @Test
    void attractionLatitude_normal_thenNoConstraintViolations() {
        // GIVEN
        // WHEN
        nearByAttraction.setAttractionLatitude(10d);
        // THEN
        String[][] errorList = {};
        testConstraintViolation.checking(nearByAttraction, errorList);
        assertThat(nearByAttraction.getAttractionLatitude()).isEqualTo(10d);
    }

    // -----------------------------------------------------------------------------------------------
    // attractionLongitude attribute
    // -----------------------------------------------------------------------------------------------
    @Test
    void attractionLongitude_normal_thenNoConstraintViolations() {
        // GIVEN
        // WHEN
        nearByAttraction.setAttractionLongitude(10d);
        // THEN
        String[][] errorList = {};
        testConstraintViolation.checking(nearByAttraction, errorList);
        assertThat(nearByAttraction.getAttractionLongitude()).isEqualTo(10d);
    }

    // -----------------------------------------------------------------------------------------------
    // userLatitude attribute
    // -----------------------------------------------------------------------------------------------
    @Test
    void userLatitude_normal_thenNoConstraintViolations() {
        // GIVEN
        // WHEN
        nearByAttraction.setUserLatitude(10d);
        // THEN
        String[][] errorList = {};
        testConstraintViolation.checking(nearByAttraction, errorList);
        assertThat(nearByAttraction.getUserLatitude()).isEqualTo(10d);
    }

    // -----------------------------------------------------------------------------------------------
    // userLongitude attribute
    // -----------------------------------------------------------------------------------------------
    @Test
    void userLongitude_normal_thenNoConstraintViolations() {
        // GIVEN
        // WHEN
        nearByAttraction.setUserLongitude(10d);
        // THEN
        String[][] errorList = {};
        testConstraintViolation.checking(nearByAttraction, errorList);
        assertThat(nearByAttraction.getUserLongitude()).isEqualTo(10d);
    }

    // -----------------------------------------------------------------------------------------------
    // distance attribute
    // -----------------------------------------------------------------------------------------------
    @Test
    void distance_normal_thenNoConstraintViolations() {
        // GIVEN
        // WHEN
        nearByAttraction.setDistance(10d);
        // THEN
        String[][] errorList = {};
        testConstraintViolation.checking(nearByAttraction, errorList);
        assertThat(nearByAttraction.getDistance()).isEqualTo(10d);
    }

    // -----------------------------------------------------------------------------------------------
    // rewardPoints attribute
    // -----------------------------------------------------------------------------------------------
    @Test
    void rewardPoints_normal_thenNoConstraintViolations() {
        // GIVEN
        // WHEN
        nearByAttraction.setRewardPoints(10);
        // THEN
        String[][] errorList = {};
        testConstraintViolation.checking(nearByAttraction, errorList);
        assertThat(nearByAttraction.getRewardPoints()).isEqualTo(10);
    }
}
