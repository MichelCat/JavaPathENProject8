package com.openclassrooms.tourguide.model;

import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * Near by attraction is business model
 *
 * @author MC
 * @version 1.0
 */

@Data
@FieldDefaults(level= AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class NearByAttraction {
    /**
     * Name of Tourist attraction,
     */
    @NotBlank
    String attractionName;
    /**
     * Tourist attraction latitude
     */
    double attractionLatitude;
    /**
     * Tourist attraction longitude
     */
    double attractionLongitude;
    /**
     * User location latitude
     */
    double userLatitude;
    /**
     * User location longitude
     */
    double userLongitude;
    /**
     * The distance in miles between the user's location and each of the attractions
     */
    double distance;
    /**
     * Reward points for visiting the attraction
     */
    int rewardPoints;
}
