![Image](src/main/resources/static/img/TourGuide.png "TourGuide")
# TourGuide

TourGuide is a Spring Boot application. It allows users to see nearby tourist attractions and get discounts on hotel stays and tickets to various shows.

Maximum response times for 100 000 users have been set. To respect this performance, the application uses CompletableFutures.

## Getting Started

These instructions allow you to run a copy of the project locally on your workstation for development and testing.

### Prerequisites

To run the TourGuide project locally, you must first install :

* Install **Java 17** : https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html
* Install **Gradle 8.4** : https://gradle.org/install/
* Install **Docker Desktop** : https://docs.docker.com/desktop/install/windows-install/
* Access to **GitLab** : https://gitlab.com/

### Installation

> Clone the Git repository to your local machine \
git clone https://gitlab.com/MichelCat/JavaPathENProject8.git

> Place yourself in the directory \
cd JavaPathENProject8/TourGuide

> Build your application with gradle \
./gradlew build

> Run the application \
./gradlew bootRun

> Retrieve the Docker image \
docker pull michelcat/tourguide:latest

> Run the Docker image \
docker run -p 8080:8080 michelcat/tourguide

## Test

> Executing unit and integration tests. And reports in directory "build/reports". \
./gradlew check

## Endpoints

[Postman file containing the APIs](./doc/TourGuide.postman_collection.json)

> The 5 tourist attractions closest to the user \
http://localhost:8080/getNearbyAttractions?userName=internalUser1

> Message Greetings \
http://localhost:8080/

> A user's last visit location \
http://localhost:8080/getLocation?userName=internalUser1

> A user's list of rewards \
http://localhost:8080/getTripDeals?userName=internalUser1

> A user's travel offers based on their preferences \
http://localhost:8080/getRewards?userName=internalUser1

## Javadoc

> Run commands for Javadoc in directory "build/docs" \
gradle javadoc

## Metric

The PerformanceIT class allows you to test the performance of the application

## Technologies

* [Java JDK 17.0.8](https://www.oracle.com/java/technologies/javase/jdk19-archive-downloads.html)
* Gradle 8.4
* [SpringBoot 3.1.1](https://spring.io/projects/spring-boot)
* Lombok 1.18.30
* Jacoco
* Slf4j
* Javadoc
* JUnit 5

## Documentations

### Architecture
![Image](src/main/resources/static/img/Architecture.png "Architecture")

The application is made up of 4 microservices:
* TourGuide : Main program
* GpSUtil : Management of places of visits and attractions
* TripPricer : Price management
* RewardsCentral : Calculation of reward points for a user and an attraction

### Class diagram
![Image](src/main/resources/static/img/classDiagram.png "classDiagram")

* User : User of the application
* UserPreferences : User preferences
* UserReward : User rewards
* Location : GPS longitude / latitude coordinate
* Attraction : Tourist attraction
* VisitedLocation : Places visited by the user
* Provider : Travel offer provider

## Auteurs

* **OpenClassrooms student**
