# TourGuide

TourGuide is a Spring Boot application. It allows users to see nearby tourist attractions and get discounts on hotel stays and tickets to various shows.

Click on directory "TourGuide" to see the README